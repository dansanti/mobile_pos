import re
from odoo import _, api, models, fields
from odoo.exceptions import AccessDenied
import uuid
from datetime import datetime, timedelta
import qrcode
from base64 import b64encode
from io import BytesIO
import json
import logging
_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    def mobile_token_qr_image(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        db = self.env.cr.dbname
        for r in self:
            if not r.mobile_token or fields.Datetime.from_string(r.mobile_token_expire_date) < fields.Datetime.from_string(fields.Datetime.now()):
                r._generate_token()
            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.constants.ERROR_CORRECT_L,
                box_size=10,
                border=4,
            )
            data = {
                'url': base_url,
                'database': db,
                'user': self.env.user.login,
                'token': self.mobile_token,
            }
            qr.add_data(json.dumps(data))
            qr.make(fit=True)
            img = qr.make_image()
            temp = BytesIO()
            img.save(temp, format="PNG")
            b64_img = b64encode(temp.getvalue())
            r.mobile_token_qr = b64_img

    mobile_token = fields.Text(
        string="Login Token for mobile pos",
        copy=False)
    mobile_token_expire_date = fields.Datetime(
        string="Mobile Token Expire Time",
        copy=False
    )
    mobile_token_qr = fields.Binary(
        string="QR for login",
        compute="mobile_token_qr_image"
    )

    def _generate_token(self):
        self.write({
            "mobile_token": str(uuid.uuid4()),
            "mobile_token_expire_date": (datetime.now() + timedelta(minutes=10))
        })

    @api.multi
    def generate_token(self):
        self._generate_token()

    @api.model
    def check_credentials(self, password):
        try:
            return super(ResUsers, self).check_credentials(password)
        except AccessDenied:
            res = self.sudo().search([
                ('id', '=', self.env.uid),
                ('mobile_token', '=', password),
                ('mobile_token_expire_date', '>=', fields.Datetime.now())])
            if not res:
                raise
