odoo.define('web.MobilePOS', function (require) {
"use strict";
  var UserMenu = require('web.UserMenu');

  UserMenu.include({
    /**
     * @private
     */
    _onMenuQrlogin: function () {
        var self = this;
        var session = this.getSession();
        this.trigger_up('clear_uncommitted_changes', {
            callback: function () {
                self._rpc({
                        route: "/web/action/load",
                        params: {
                            action_id: "mobile_pos.action_res_users_qr_login_my",
                        },
                    })
                    .done(function (result) {
                        result.res_id = session.uid;
                        self.do_action(result);
                    });
            },
        });
    },
  })


})
